export interface Payment {
  id: number;
  amount: number;
  description: string;
}

export interface PaymentState {
    payments: Payment[]
}
