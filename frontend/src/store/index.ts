import Vue from "vue";
import Vuex, { Store } from "vuex";
import axios from "axios";
import { Payment, PaymentState } from "@/types/index";

Vue.use(Vuex);

const state: PaymentState = {
  payments: [],
};

// TODO: refactor api calls and mutations
// TODO: configure the API URL globally

export default new Vuex.Store({
  state: state,
  mutations: {
    setPayments(state, payments) {
      state.payments = payments;
    },
    addPayment(state, payment: Payment) {
      state.payments.push(payment);
    },
  },
  actions: {
    getPayments(context) {
      axios.get("http://127.0.0.1:8000/payments").then((response) => {
        console.log(response);
        context.commit("setPayments", response.data.data);
      });
    },
    createPayment(context, { amount, description }) {
      return new Promise((resolve, reject) => {
        axios
          .post("http://127.0.0.1:8000/payments", {
            amount: amount,
            description: description,
          })
          .then((response) => {
            context.commit("addPayment", response.data.data);
            resolve(response);
          })
          .catch((error) => {
            reject(error.response.data.errors);
          });
      });
    },
  },
  getters: {
    payments: (state) => {
      return state.payments;
    },
  },
  modules: {},
});
