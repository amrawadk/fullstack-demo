# Full Stack Demo

This is a demo of a full stack app, that uses:

- Flask: for the api
- Vue: for the frontend
- Postgres: for the database
- docker-compose: for linking them together

This repo serves as a playground for experimenting with features, and also exploring blockchain integration into existing applications

## Running the Demo

To run the demo, you only need to do a single command : )

```
docker-compose up
```

Once the app is running, you can view & create payments, this is talking to the api and persisted in the database.

![Captured GIF](./docs/sample.gif)
