import random
import pytest

from backend.main import app
from backend.extensions import db


@pytest.fixture
def database():
    """TODO: improve this, we could use transactions to not have to recreate the tables for each test.
    """
    with app.app_context():
        db.create_all()
        yield
        db.drop_all()


@pytest.fixture
def client(database):
    yield app.test_client()


# @pytest.mark.skip(reason="not implemented yet")
def test_createPayment_shouldReturnCreatedPayment(client, request):
    body = {"amount": random.randint(10, 1000), "description": request.node.name}
    response = client.post("/payments", json=body)

    assert response.status_code == 200
    assert response.json["data"]["amount"] == body["amount"]
    assert response.json["data"]["description"] == body["description"]


def test_createPayment_whenAmountIsFloat_shouldReturn400(client, request):
    body = {"amount": random.random(), "description": request.node.name}
    response = client.post("/payments", json=body)

    assert response.status_code == 400
    assert "amount" in response.json["errors"]


def test_createPayment_whenInvalidDescription_shouldReturn400(client, request):
    body = {"amount": random.randint(1, 1000), "description": 2}
    response = client.post("/payments", json=body)

    assert response.status_code == 400
    assert "description" in response.json["errors"]


def test_getAllPayments_shouldReturnPayments(client, request):
    # TODO: use factoryboy for seeding the database with test objects
    body = {"amount": random.randint(10, 1000), "description": request.node.name}
    response = client.post("/payments", json=body)

    response = client.get("/payments")
    assert response.status_code == 200
    assert len(response.json["data"]) == 1
    assert response.json["data"][0]["description"] == body["description"]
    assert response.json["data"][0]["amount"] == body["amount"]


def test_getPaymentById_shouldReturnPayment(client, request):
    body = {"amount": random.randint(10, 1000), "description": request.node.name}
    response = client.post("/payments", json=body)
    payment_id = response.json["data"]["id"]

    response = client.get(f"/payments/{payment_id}")
    assert response.status_code == 200
    assert response.json["data"]["description"] == body["description"]
    assert response.json["data"]["amount"] == body["amount"]


def test_getPaymentById_whenNoneExists_shouldReturn404(client, request):
    payment_id = random.randint(1, 1000)
    response = client.get(f"/payments/{payment_id}")
    assert response.status_code == 404
