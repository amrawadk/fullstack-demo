from flask import jsonify
from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field

from backend.models import Payment


class CustomBaseSchema(SQLAlchemySchema):
    def jsonify(self, *args, **kwargs):
        """Returns a top level key to handle serializing collections
        ref: https://stackoverflow.com/q/19623339/7178540
        """
        return jsonify({"data": self.dump(*args, **kwargs)})


class PaymentSchema(CustomBaseSchema):
    class Meta:
        model = Payment
        fields = ["id", "amount", "description"]
        load_instance = True


payment_schema = PaymentSchema()


class PaymentCreateSchema(CustomBaseSchema):
    class Meta:
        model = Payment

    amount = auto_field(required=True, strict=True)  # strict to not accept floats
    description = auto_field(required=True)


payment_create_schema = PaymentCreateSchema()
