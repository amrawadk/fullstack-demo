import os
from flask import Flask, jsonify, request, abort

from marshmallow.exceptions import ValidationError

from backend.config import ConfigFactory
from backend.extensions import register_extensions, db
from backend.views import payments_api

app = Flask(__name__)

# TODO: create a `create_app` function
config = os.environ["APP_SETTINGS"] if "APP_SETTINGS" in os.environ else "Dev"
app.config.from_object(ConfigFactory.of(config))
register_extensions(app)
app.register_blueprint(payments_api)


# TODO: find a better place for configuring custom error handlers
@app.errorhandler(ValidationError)
def handle_marshmallow_validation_error(e):
    return jsonify({"errors": e.messages}), 400
