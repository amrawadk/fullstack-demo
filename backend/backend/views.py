from flask import Blueprint, request

from backend.extensions import db
from backend.models import Payment
from backend.schemas import payment_schema, payment_create_schema


payments_api = Blueprint("payments_api", __name__)


@payments_api.route("/payments", methods=["POST"])
def create_payment():
    data = payment_create_schema.load(request.json, session=db.session)

    payment = Payment(**data)

    db.session.add(payment)
    db.session.commit()

    return payment_schema.jsonify(payment)


@payments_api.route("/payments", methods=["GET"])
def get_payment():
    payments = Payment.query.all()
    return payment_schema.jsonify(payments, many=True)


@payments_api.route("/payments/<int:payment_id>", methods=["GET"])
def get_payment_by_id(payment_id):
    payment = Payment.query.get_or_404(payment_id)
    return payment_schema.jsonify(payment)
