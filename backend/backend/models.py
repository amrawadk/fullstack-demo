from backend.extensions import db


class Payment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    amount = db.Column(db.Integer, nullable=False)  # cents
    description = db.Column(db.String(120), nullable=False)  # cents
