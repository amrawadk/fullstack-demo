from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_cors import CORS


db = SQLAlchemy()
ma = Marshmallow()
cors = CORS()


def register_extensions(app):
    db.init_app(app)
    ma.init_app(app)
    cors.init_app(app)

    with app.app_context():
        # Make sure all tables are up to do
        # TODO: this should run database migrations instead
        db.create_all()
